const express = require('express');
const app = express();
const axios = require('axios');
const port = 8080;

// Serve static files from the root directory
app.use(express.static(__dirname));

app.get('/user-service', async (req, res) => {
    try {
        const response = await axios.get('http://user-service');
        res.json(response.data);
    } catch (error) {
        res.status(500).send(error.toString());
    }
});

app.get('/product-service', async (req, res) => {
    try {
        const response = await axios.get('http://product-service');
        res.json(response.data);
    } catch (error) {
        res.status(500).send(error.toString());
    }
});

app.listen(port, () => {
    console.log(`Web app listening at http://localhost:${port}`);
});