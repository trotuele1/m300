# 28.06 2024 - last day

## What I did today
The final day involved a comprehensive review and enhancement of the microservices architecture. This included ensuring all services were running correctly, implementing advanced networking features, and integrating monitoring and alerting systems. The following steps and checks were performed to ensure the system's stability and functionality.
Tasks Completed
### 1. Review of All Services

- Objective: Ensure all microservices are running as expected.
    - Actions:
        - Checked the status of all microservices using1 kubectl get pods -n microservices.
        - Verified that all pods were in the "Running" state and there were no restarts or failures.
        - Examined logs for each service to ensure there were no errors or warnings.

  ### 2. Database Configuration and Security

  - Objective: Secure and verify the PostgreSQL database setup.
      - Actions:
          - Ensured the PostgreSQL database deployment and service configurations were correct.
          - Verified the database connection string in the application configurations.
          - Confirmed the database was accessible and functioning by running a few test queries.
          - Checked that the database credentials were stored securely using Kubernetes secrets.



  ````yaml

  apiVersion: v1
  kind: Secret
  metadata:
    name: postgres-secret
    namespace: microservices
  type: Opaque
  data:
    POSTGRES_DB: YXBwZGI=
    POSTGRES_USER: dXNlcg==
    POSTGRES_PASSWORD: cGFzc3dvcmQ=
  ````


  `````yaml

  env:
  - name: POSTGRES_DB
    valueFrom:
      secretKeyRef:
        name: postgres-secret
        key: POSTGRES_DB
  - name: POSTGRES_USER
    valueFrom:
      secretKeyRef:
        name: postgres-secret
        key: POSTGRES_USER
  - name: POSTGRES_PASSWORD
    valueFrom:
      secretKeyRef:
        name: postgres-secret
        key: POSTGRES_PASSWORD
  `````
  ### 3. Integration of Prometheus and Grafana

  - Objective: Ensure monitoring and alerting are set up correctly.
      - Actions:
          - Verified that Prometheus was scraping metrics from all microservices.
          - Confirmed the Prometheus configuration was correct and targets were being scraped without issues.
          - Reviewed Grafana dashboards to ensure metrics were displayed correctly.
          - Set up alerts in Prometheus to notify via Slack in case of any critical issues.

  #### Example Prometheus Configuration:

  ```yaml

  global:
    scrape_interval: 15s
  scrape_configs:
  - job_name: 'microservices'
    static_configs:
    - targets: ['frontend-metrics.microservices.svc.cluster.local:8000', 'productservice-metrics.microservices.svc.cluster.local:5001', 'paymentservice-metrics.microservices.svc.cluster.local:5002', 'userservice-metrics.microservices.svc.cluster.local:5000']
  alerting:
    alertmanagers:
    - static_configs:
      - targets:
        - 'alertmanager.monitoring.svc.cluster.local:9093'
  ```
  ### 4. AlertManager and Slack Integration

  - Objective: Ensure alerts are sent to Slack.
      - Actions:
          - Configured AlertManager with the Slack webhook URL.
          - Created alert rules in Prometheus for critical conditions.
          - Verified that alerts were triggered and sent to Slack appropriately.

  #### Example AlertManager Configuration:

  `````yaml

  global:
    resolve_timeout: 5m
  route:
    receiver: 'slack-notifications'
  receivers:
  - name: 'slack-notifications'
    slack_configs:
    - send_resolved: true
      api_url: 'https://hooks.slack.com/services/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'
      channel: '#alerts'
      username: 'alertmanager'
      icon_emoji: ':warning:'
  `````


  ### 5. Final Verification

  - Objective: Ensure everything is functioning correctly for the final deployment.
      - Actions:
          - Conducted a full test of the application, including user registration, login, and payment processing.
          - Monitored the system for any errors or performance issues.
          - Verified that alerts were correctly sent to Slack for any issues encountered.

  By the end of the day, all services were reviewed, and the final checks confirmed that the microservices architecture was stable, secure, and fully operational.