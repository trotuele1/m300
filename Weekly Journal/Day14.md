# 01.07.2024 - Last Day Part 2

## What I did today
The final day involved a comprehensive review and enhancement of the microservices architecture. This included ensuring all services were running correctly, implementing advanced networking features, and integrating monitoring and alerting systems. The following steps and checks were performed to ensure the system's stability and functionality.

### 1. Review of All Services

- Objective: Ensure all microservices are running as expected.
    - Actions:
        - Checked the status of all microservices using kubectl get pods -n microservices.
        - Verified that all pods were in the "Running" state and there were no restarts or failures.
        - Examined logs for each service to ensure there were no errors or warnings.

### 2. SonarQube Setup and Integration

- Objective: Implement SonarQube for code quality analysis.
    - Actions:
        - Installed SonarQube and SonarScanner.
        - Configured SonarQube with necessary properties for the project.
        - Deployed SonarQube to Kubernetes and resolved port conflicts with Prometheus by adjusting service configurations.
        - Created a new secret for SonarQube if required and ensured it was correctly referenced in the deployment files.

### Example SonarQube Configuration:

```yaml

apiVersion: v1
kind: Secret
metadata:
  name: sonarqube-secret
  namespace: microservices
type: Opaque
data:
  SONARQUBE_JDBC_URL: <base64_encoded_jdbc_url>
  SONARQUBE_JDBC_USERNAME: <base64_encoded_username>
  SONARQUBE_JDBC_PASSWORD: <base64_encoded_password>
```

````yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: sonarqube
  namespace: microservices
spec:
  replicas: 1
  selector:
    matchLabels:
      app: sonarqube
  template:
    metadata:
      labels:
        app: sonarqube
    spec:
      containers:
      - name: sonarqube
        image: sonarqube:9.6-community
        ports:
        - containerPort: 9001  # Changed from 9000 to avoid conflict
        env:
        - name: SONAR_JDBC_URL
          valueFrom:
            secretKeyRef:
              name: sonarqube-secret
              key: SONARQUBE_JDBC_URL
        - name: SONAR_JDBC_USERNAME
          valueFrom:
            secretKeyRef:
              name: sonarqube-secret
              key: SONARQUBE_JDBC_USERNAME
        - name: SONAR_JDBC_PASSWORD
          valueFrom:
            secretKeyRef:
              name: sonarqube-secret
              key: SONARQUBE_JDBC_PASSWORD
````

### 3. Running SonarScanner

- Objective: Execute SonarScanner for code analysis.
    - Actions:
        - Configured SonarScanner properties to point to the local SonarQube instance.
        - Executed SonarScanner from the project root, ensuring the correct path setup.
        - Resolved issues with SonarScanner not recognizing the correct project path by adjusting the command line execution context.
        - Verified the analysis results in the SonarQube dashboard.

### 4. Final Verification

- Objective: Ensure everything is functioning correctly for the final deployment.
    - Actions:
        - Conducted a full test of the application, including user registration, login, and payment processing.
        - Monitored the system for any errors or performance issues.
        - Verified that alerts were correctly sent to Slack for any issues encountered.

### 5. Issues with PATH

- Objective: Resolve any environment path issues for tools like SonarScanner.
    - Actions:
        - Ensured the SonarScanner path was correctly added to the system's PATH environment variable.
        - Verified the correct setup by running the sonar-scanner.bat command successfully from the project root directory.

By the end of the day, all services were reviewed, and the final checks confirmed that the microservices architecture was stable, secure, and fully operational.
