# 11. June 2024

## What did I do

### 1. Initial Setup and Prometheus Metrics Integration

- **Problem**: I needed to integrate Prometheus metrics into my microservices.
- **Solution**: Added the Prometheus Flask Exporter to the backend services and configured the metrics endpoint properly.

```python
from prometheus_flask_exporter import PrometheusMetrics
app = Flask(__name__)
metrics = PrometheusMetrics(app, path='/metrics')

if not hasattr(app, 'metrics'):
    app.metrics = metrics
```

### 2. Configuring Docker and Kubernetes
- **Problem**: Ensuring that Docker images are correctly built and Kubernetes deployments are properly configured.
- **Solution**: Created Dockerfiles and Kubernetes deployment/service configurations for each microservice.

```dockerfile
FROM python:3.8-slim

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000

CMD ["python", "app.py"]
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: userservice
  namespace: microservices
spec:
  replicas: 1
  selector:
    matchLabels:
      app: userservice
  template:
    metadata:
      labels:
        app: userservice
    spec:
      containers:
      - name: userservice
        image: trotuele/userservice:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 5000
        livenessProbe:
          httpGet:
            path: /metrics
            port: 5000
          initialDelaySeconds: 10
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: /metrics
            port: 5000
          initialDelaySeconds: 10
          periodSeconds: 10
```

### Reflection
I think I did a really good job today. All the microservices are setup in Prometheus and can read data from it. I was confused why it didn't work but that was because I wasn't using ClusterIP as a service. This made the process much easier and added a good learning curve.