# 31. May 2024

## What did I do

Today I was working on my planning. I wrote down what I want to do and what I want to get done. I created a timetable for my project and a risk matrix, since there are a lot of risks ahead of me. This took me a bit but I got it done after a while. 
When I was done I started working on setting everything up, what I mean by that is downloading everything and getting familiar with it. It was very hard. A lot of the stuff I tried didn't work. I used the kubectl dashboard which was useful but the errors weren't helpful. In the end my biggest problem was accessing the container. I made a simple container and ran but couldn't access it. The reason for this was because I misconfigured the port forwarding, which I normally don't forget but it happened. In the end, everything worked out. There were some rocks in the way but I got through them.

## Reflection

I think I did a good job managing the problems. I stayed calm and made sure to do everything carefully, I even worked a little more after school. I am proud of what I did and am excited for the next day.