# 5. June 2024

## What did I do

Today was the most problem solving and most aggrevating day so far. I ran into a lot of problems with testing my dependencies like Flask. It took me well over 5 hours to fix the issue and the issue was such a simple mistake. When I created the deployment files I forgot to add my Docker Hub username. It would always push to the local environemnt which isn't the goal and didn't have permission. I figured it out but there were other issues. I had issues with the Werkzeug dependency because it had misconfigurations with the Flask. I got it to work and I am happy. Next time I hope there will be less problems.

## Reflection

I did a good job in being patient, I tried my best to learn from my mistakes mentioned above but all I can do is learn. I am looking forward for the next issues and learning curves.