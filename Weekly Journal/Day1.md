# 17. May 2024

## What did I do

Today was the first day and the start of our project for M300. I paid attention to the teacher explaining and was already thinking about what my project could be. I already planned my first project idea which can be found [here](https://gitlab.com/trotuele1/m300/-/blob/main/Project/Concept/1.%20Project%20Idea.md?ref_type=heads). I then started to scramble ideas and browse for the necessary applications. A while ago I set up a media server which used Grafana so I could use that.<br/>
I wrote and documented my project idea. I had to relearn the markdown syntaxes. Thankfully there was a Cheat Sheet for this. I also browsed the extensions tab that might be useful for Markdown and I found the Footnote Extension [^1] which seems useful. 

## Reflection

I think I was consistently focused throughout this afternoon, I will try to keep working on this over the weekend. Next time I should bring my own charger...


## Sources

[^1]: Markdown Footnotes (04.17.2024). ["Markdown Footnotes"](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-footnotes)