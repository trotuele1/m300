# 14. June 2024

## What did I do

### 1. Attempted to configure SSL on PostgreSQL Database

- **Problem**: I wanted to configure my Database with SSL but the database crashed.
- **Solution**: Rebuilt the database from scratch and used pods to reset the config on the database.

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: update-postgres-config
  namespace: microservices
spec:
  template:
    spec:
      containers:
      - name: update-config
        image: postgres:13
        command: ["/bin/sh", "-c"]
        args:
        - |
          cp /server.crt /var/lib/postgresql/data/server.crt &&
          cp /server.key /var/lib/postgresql/data/server.key &&
          echo "ssl = on" >> /var/lib/postgresql/data/postgresql.conf &&
          echo "ssl_cert_file = '/var/lib/postgresql/data/server.crt'" >> /var/lib/postgresql/data/postgresql.conf &&
          echo "ssl_key_file = '/var/lib/postgresql/data/server.key'" >> /var/lib/postgresql/data/postgresql.conf &&
          echo "hostssl all all 0.0.0.0/0 md5" >> /var/lib/postgresql/data/pg_hba.conf
        volumeMounts:
        - name: postgres-storage
          mountPath: /var/lib/postgresql/data
        - name: certs
          mountPath: /
      restartPolicy: OnFailure
      volumes:
      - name: postgres-storage
        persistentVolumeClaim:
          claimName: postgres-pvc
      - name: certs
        secret:
          secretName: trot
  backoffLimit: 4

```


### Reflection
Today was mostly rebuilding my database from scratch. I wanted to implement SSL into my database but it unfortunately crashed after I configured the SSL files. This caused my PVC file to kill itself and couldn't deploy the pods again. This meant that I had to rebuild my database. Luckily, this didn't cause too much damage since all I had to do was rebuild it. 