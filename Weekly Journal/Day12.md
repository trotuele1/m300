# 21. June 2024

## What did I do

 ### 1. Prometheus and Alertmanager Setup

#### Configuration:
- Successfully set up Prometheus to monitor various microservices.
- Integrated Alertmanager with Prometheus to handle alerts.

- Alert Testing:
  - Configured alert rules in Prometheus to detect service downtimes.
  - Verified that alerts are properly sent to Alertmanager and then to Slack, ensuring real-time notifications for critical issues.
  - **Outcome:** The setup was successful, and alerts were triggered and resolved as expected, enhancing incident response capabilities.

### 2. Database Security Enhancement

#### Configuration:
- Secured the PostgreSQL database by integrating Kubernetes secrets to handle sensitive information like database credentials.
- Implemented environment variables to manage database connections securely.
  - **Outcome**: Successfully encrypted the database connections, ensuring that sensitive data is securely managed and transmitted.

##### Detailed Steps Taken
- Prometheus and Alertmanager
- Configured Prometheus:
  - Defined scrape configurations to monitor various microservices.
  - Added alerting rules to detect service downtimes.

```yaml

apiVersion: v1
kind: ConfigMap
metadata:
  name: prometheus-server-conf
  namespace: monitoring
data:
  prometheus.yml: |
    global:
      scrape_interval: 15s
    scrape_configs:
    - job_name: 'microservices'
      static_configs:
      - targets: ['frontend-metrics.microservices.svc.cluster.local:8000', 'productservice-metrics.microservices.svc.cluster.local:5001', 'paymentservice-metrics.microservices.svc.cluster.local:5002', 'userservice-metrics.microservices.svc.cluster.local:5000']
    rule_files:
    - "alert.rules"
````

````yaml

groups:
- name: example
  rules:
  - alert: InstanceDown
    expr: up == 0
    for: 1m
    labels:
      severity: critical
    annotations:
      description: "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 1 minute."
      summary: "Instance {{ $labels.instance }} down"
````
#### 3. Set Up Alertmanager:

- Created and applied the Alertmanager ConfigMap to manage alert routing.
- Configured Alertmanager to send notifications to a specified Slack channel using a webhook URL.


```````yaml

    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: alertmanager-config
      namespace: monitoring
    data:
      alertmanager.yml: |
        global:
          resolve_timeout: 5m
        route:
          receiver: 'slack-notifications'
        receivers:
        - name: 'slack-notifications'
          slack_configs:
          - api_url: 'https://hooks.slack.com/services/YOUR/SLACK/WEBHOOK'
            channel: '#alerts'
            send_resolved: true

``````````

#### 4. Tested Alerts:
- Simulated service downtime to test the alerting mechanism.
- Verified that alerts transitioned from PENDING to FIRING and notifications were received in Slack.

#### 5. Database Security
- Using Kubernetes Secrets:
  - Created Kubernetes secrets to store sensitive information such as database passwords.
  - Modified the PostgreSQL deployment to use these secrets.

    
````yaml

apiVersion: v1
kind: Secret
metadata:
  name: postgres-secrets
  namespace: microservices
type: Opaque
data:
  POSTGRES_DB: YXBwZGI=   # base64 encoded 'appdb'
  POSTGRES_USER: dXNlcg== # base64 encoded 'user'
  POSTGRES_PASSWORD: cGFzc3dvcmQ= # base64 encoded 'password'
````
````````yaml

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: postgres
      namespace: microservices
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: postgres
      template:
        metadata:
          labels:
            app: postgres
        spec:
          containers:
          - name: postgres
            image: postgres:13
            env:
            - name: POSTGRES_DB
              valueFrom:
                secretKeyRef:
                  name: postgres-secrets
                  key: POSTGRES_DB
            - name: POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  name: postgres-secrets
                  key: POSTGRES_USER
            - name: POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: postgres-secrets
                  key: POSTGRES_PASSWORD
            volumeMounts:
            - name: postgres-storage
              mountPath: /var/lib/postgresql/data
          volumes:
          - name: postgres-storage
            persistentVolumeClaim:
              claimName: postgres-pvc
````````

By following these steps, I ensured that the microservices are monitored effectively, alerts are handled promptly, and sensitive data is securely managed.