# 9. June 2024

## What did I do?

### 1. Initial Setup and CORS Issues

- **Problem**: I had CORS issues when trying to connect to the microservices.  
- **Solution**: Added the Flask-CORS library to the backend services and configured CORS settings properly.

```python
from flask_cors import CORS
app = Flask(__name__)
CORS(app)
```

### 2. NodePort and LoadBalancer

- **Problem:** There was confusion about whether to use NodePort or LoadBalancer for service types.
- **Solution:** I decided to use LoadBalancer to simplify the setup and make sure services were accessible through Minikube's IP.

```YAML
apiVersion: v1
kind: Service
metadata:
  name: frontend
  namespace: microservices
spec:
  type: LoadBalancer
  selector:
    app: frontend
  ports:
    - protocol: TCP
      port: 3000
      targetPort: 3000
```

### 3. Frontend and Backend Integration

- **Problem**:  The frontend HTML files were not correctly communicating with the backend services.
- **Solution**: I Updated the HTML files to use the correct service URLs and ensured that local storage keys (username and user_id) were properly set and used.

```HTML
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order Confirmation</title>
</head>
<body>
    <h1>Order Confirmed</h1>
    <h2>Your Transaction History</h2>
    <div id="transaction-list"></div>

    <script>
        const user_id = localStorage.getItem('user_id');
        if (!user_id) {
            window.location.href = 'login.html';
        }

        console.log('Fetching transaction history...');
        fetch(`http://localhost:5002/payments/${user_id}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok ' + response.statusText);
                }
                return response.json();
            })
            .then(data => {
                console.log('Transaction history:', data);
                const transactionList = document.getElementById('transaction-list');
                if (data.length > 0) {
                    data.forEach(transaction => {
                        const transactionItem = document.createElement('div');
                        transactionItem.innerText = `Order #${transaction.order_number} - Date: ${transaction.timestamp}`;
                        transactionList.appendChild(transactionItem);
                    });
                } else {
                    transactionList.innerText = 'No transactions found';
                }
            })
            .catch(error => console.error('Error fetching transaction history:', error));
    </script>
</body>
</html>
```

### 4. Database Migration and Schema Issues

- **Problem**: The payment table in the database was missing required columns (order_number and timestamp).
- **Solution**: Updated the database schema directly using PostgreSQL commands to add the missing columns.

```SQL
ALTER TABLE payment ADD COLUMN order_number VARCHAR(20);
ALTER TABLE payment ADD COLUMN timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
```

### 5. Backend Updates to Handle Payments

- **Problem**: Processing payments and fetching transaction history was not working correctly due to missing columns and incorrect logic.
- **Solution**: Updated the "app.py" file of the payment service to handle payments properly and fetch user-specific transaction history

```python
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from datetime import datetime
import random

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:password@db:5432/appdb'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)

class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    amount = db.Column(db.Float, nullable=False)
    currency = db.Column(db.String(10), nullable=False, default='USD')
    status = db.Column(db.String(20), nullable=False)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    order_number = db.Column(db.String(20), nullable=False)

with app.app_context():
    db.create_all()

@app.route('/pay', methods=['POST'])
def process_payment():
    data = request.get_json()
    user_id = data.get('user_id')
    amount = data.get('amount')
    currency = data.get('currency', 'USD')

    user = User.query.get(user_id)
    if user:
        try:
            order_number = f"ORD{random.randint(1000, 9999)}"
            new_payment = Payment(
                user_id=user_id,
                amount=amount,
                currency=currency,
                status='Completed',
                timestamp=datetime.utcnow(),
                order_number=order_number
            )
            db.session.add(new_payment)
            db.session.commit()
            return jsonify({"message": "Payment successful", "order_number": order_number, "payment_id": new_payment.id}), 201
        except Exception as e:
            db.session.rollback()
            return jsonify({"message": "Payment failed due to server error", "error": str(e)}), 500
    return jsonify({"message": "User not found"}), 404

@app.route('/payments/<int:user_id>', methods=['GET'])
def get_payments(user_id):
    try:
        payments = Payment.query.filter_by(user_id=user_id).all()
        return jsonify([{
            'order_number': payment.order_number,
            'amount': payment.amount,
            'currency': payment.currency,
            'timestamp': payment.timestamp
        } for payment in payments]), 200
    except Exception as e:
        return jsonify({"message": "Error fetching payments", "error": str(e)}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)
```


## Reflection
I worked for 6 hours on this. I got frustrated a lot and I was close to trying something new, I looked online and couldn't find anything unfortunately... I resorted to using my own logic and thinking, which made things worse sometimes, and it worked out! In the future I need to remain calm and make sure that I analyze the issue. Minikube has the best error codes because they are exact and easy to access. Using logfiles makes solving the issue easier.