# 6. June 2024

## What did I do

On this day, I was working on the backend of the platform. It was working fine, I had a few errors and it got annoying with time but it worked. For now, I decided I was going to use docker compose to push everything, because that makes everything easier. Working on the backend really takes time and care. Since it's made that the frontend fetches data from the backend it needs to be structured and well maintained. I learned a lot and discovered new dependencies like PostGRE. I heard of it but now I learned how to implement into my project. 


## Reflection

I did very well today. I was able to create dynamic carts, meaning that each user has their own cart. At the beginning everybody had the same cart. Now it's like each user is bound to a cart. So there is no cart sharing, which makes it look more professional. The next steps are implementing a payment service and order service. After that, I will try to monitor everything in Prometheus and Grafana and in the end use kubernetes for everything. 