# 05. July 2024

## What did I do

I handed in my project at around 13:45 today. I checked everything again and assured everything was running and working as intended. After I handed my project, I helped a few colleagues with their environment and solved some issues.

## Reflection

I would like to thank my coworker for helping me with this project. He pointed out that I should add SonarScanner to my project, I added it in and configured it. This project taught me a lot of valuable lessons, so does every other project, but I felt like this one worked the best. I applied all the knowledge from previous modules to this one and it went great. Of course, there was a few mistakes or learning curves but that is what I like. Improvement during this project was immense, at first I didn't really know how I should configure my Dockerfile to completely advancing them and optimizing them. This definitely isn't my last project but this one was by far one of my most successful.
