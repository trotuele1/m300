# 6. June 2024

## What did I do

Today was really bad. I wanted to move my environment to minikube and kubernetes... nothing worked. I am stuck, I am going to move the NodePort files to LoadBalancer and see what I can do. Exposing them with the minikube IP seems impossible and too much for me.

## Reflection

I had a lot of issues with CORS policies, they never would route to the proper Service and would constantly crash or not work. I fixed that issue at least but the minikube IP access still persists. I have no idea how I am going to fix it but I will try my best and see what I can do.