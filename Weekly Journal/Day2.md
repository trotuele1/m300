# 19. May 2024

## What did I do

Today I focused on how each component or service works with my environment. I did lots of research and read a lot of documentations on the topic, they can be found in the Theory part. [^1]<br/>
Since I will be primarly using markdown, I need to learn some new syntaxes. I learned how to use indentation which can be found in the cheat mentioned before [^2]. I had some issues with it, like if I wanted to use a group intendation, it would cause issues. The way I fixed that is applying proper spacing in my code.

## Reflection

In my opinion, I worked well and focused. My goal was to finish writing the theory and I managed to reach my goal. I just need to remember to work consistent and organized. If I work disorganized or inconsitent, it will leave gaps in my project which won't help me. Next Friday I will work on the planning, how each microservice will communicate with my environment in praxis.

## Sources

[^1]: What is a microservice? (05.25.2024). ["What is a microservice?"](https://gitlab.com/trotuele1/m300/-/blob/main/Project/2.%20Theory.md?ref_type=heads)
