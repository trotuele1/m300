# Commands Library

## Docker
```bash
docker build --no-cache -t trotuele/userservice ./UserService
docker build --no-cache -t trotuele/productservice ./ProductService
docker build --no-cache -t trotuele/paymentservice ./PaymentService
docker build --no-cache -t trotuele/frontend ./FrontendService
```

```bash
docker-compose up --build
docker compose down
```

```bash
docker push trotuele/frontend
docker push trotuele/userservice
docker push trotuele/productservice
docker push trotuele/paymentservice
```

## Kubectl
```bash
kubectl get services -n microservices
kubectl get pods -n microservices
kubectl delete pod -l app=paymentservice -n microservices
```

```bash
kubectl apply -f postgres-deployment.yaml
kubectl apply -f postgres-pvc.yaml
kubectl apply -f postgres-service.yaml 
kubectl apply -f postgres-secrets.yaml # optional but adds layer of security
kubectl apply -f userservice-deployment.yaml
kubectl apply -f userservice-service.yaml 
kubectl apply -f productservice-deployment.yaml
kubectl apply -f productservice-service.yaml 
kubectl apply -f paymentservice-deployment.yaml
kubectl apply -f paymentservice-service.yaml 
kubectl apply -f frontend-deployment.yaml
kubectl apply -f frontend-service.yaml
```

## Minikube
```bash
minikube ip
minikube start
minikube stop
minikube status
minikube dashboard
minikube tunnel
```

## Setting PATH
```powershell
New-Item -Path 'c:\' -Name 'minikube' -ItemType Directory -Force
Invoke-WebRequest -OutFile 'c:\minikube\minikube.exe' -Uri 'https://github.com/kubernetes/minikube/releases/latest/download/minikube-windows-amd64.exe' -UseBasicParsing

$oldPath = [Environment]::GetEnvironmentVariable('Path', [EnvironmentVariableTarget]::Machine)
if ($oldPath.Split(';') -inotcontains 'C:\minikube'){
  [Environment]::SetEnvironmentVariable('Path', $('{0};C:\minikube' -f $oldPath), [EnvironmentVariableTarget]::Machine)
}
