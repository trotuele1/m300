from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
CORS(app)

if not hasattr(app, 'metrics'):
    metrics = PrometheusMetrics(app, path='/metrics')
    app.metrics = metrics

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:password@db:5432/appdb'
db = SQLAlchemy(app)

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    price = db.Column(db.String(20), nullable=False)

with app.app_context():
    db.create_all()

# Sample products data
products = [
    {"id": 1, "name": "Product 1", "price": "$10"},
    {"id": 2, "name": "Product 2", "price": "$20"},
    {"id": 3, "name": "Product 3", "price": "$30"},
]

@app.route('/products', methods=['GET'])
def get_products():
    return jsonify({"products": products})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
