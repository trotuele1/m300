from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
CORS(app)
metrics = PrometheusMetrics(app, path='/metrics')  # Ensure the /metrics path is specified

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:password@db:5432/appdb'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)

class CartItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    product_id = db.Column(db.Integer, nullable=False)
    product_name = db.Column(db.String(80), nullable=False)
    product_price = db.Column(db.String(20), nullable=False)

with app.app_context():
    db.create_all()

@app.route('/register', methods=['POST'])
def register_user():
    user_data = request.get_json()
    username = user_data.get('username')
    password = user_data.get('password')

    if not username or not password:
        return jsonify({"message": "Username and password are required"}), 400

    if User.query.filter_by(username=username).first():
        return jsonify({"message": "User already exists"}), 400

    new_user = User(username=username, password=password)
    db.session.add(new_user)
    db.session.commit()

    return jsonify({"message": "User registered successfully"}), 201

@app.route('/login', methods=['POST'])
def login_user():
    user_data = request.get_json()
    username = user_data.get('username')
    password = user_data.get('password')

    user = User.query.filter_by(username=username, password=password).first()
    if user:
        return jsonify({"message": "Login successful", "username": user.username, "user_id": user.id}), 200

    return jsonify({"message": "Invalid credentials"}), 401

@app.route('/cart', methods=['GET', 'POST', 'DELETE'])
def manage_cart():
    if request.method == 'GET':
        username = request.args.get('username')
        user = User.query.filter_by(username=username).first()
        if user:
            cart_items = CartItem.query.filter_by(user_id=user.id).all()
            return jsonify([{
                'product_id': item.product_id,
                'product_name': item.product_name,
                'product_price': item.product_price
            } for item in cart_items]), 200
        return jsonify([]), 200

    elif request.method == 'POST':
        data = request.get_json()
        username = data.get('username')
        product_id = data.get('product_id')
        product_name = data.get('product_name')
        product_price = data.get('product_price')

        user = User.query.filter_by(username=username).first()
        if user:
            new_item = CartItem(
                user_id=user.id,
                product_id=product_id,
                product_name=product_name,
                product_price=product_price
            )
            db.session.add(new_item)
            db.session.commit()
            return jsonify({"message": "Item added to cart"}), 201
        return jsonify({"message": "User not found"}), 404
    
    elif request.method == 'DELETE':
        data = request.get_json()
        username = data.get('username')
        product_id = data.get('product_id')

        user = User.query.filter_by(username=username).first()
        if user:
            cart_item = CartItem.query.filter_by(user_id=user.id, product_id=product_id).first()
            if cart_item:
                db.session.delete(cart_item)
                db.session.commit()
                return jsonify({"message": "Item removed from cart"}), 200
        return jsonify({"message": "Item not found"}), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
