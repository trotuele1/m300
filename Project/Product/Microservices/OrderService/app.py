from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
CORS(app)
metrics = PrometheusMetrics(app)


app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:password@db:5432/appdb'
db = SQLAlchemy(app)

class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    product_id = db.Column(db.Integer, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    order_number = db.Column(db.String(20), nullable=False)

@app.before_first_request
def create_tables():
    db.create_all()

@app.route('/orders', methods=['POST'])
def create_order():
    data = request.get_json()
    order = Order(user_id=data['user_id'], product_id=data['product_id'], quantity=data['quantity'], order_number=data['order_number'])
    db.session.add(order)
    db.session.commit()
    return jsonify({'message': 'Order created'}), 201

@app.route('/orders', methods=['GET'])
def get_orders():
    orders = Order.query.all()
    return jsonify([{'user_id': order.user_id, 'product_id': order.product_id, 'quantity': order.quantity, 'order_number': order.order_number} for order in orders]), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003)
