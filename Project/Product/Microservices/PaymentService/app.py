from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from prometheus_flask_exporter import PrometheusMetrics
from datetime import datetime
import random

app = Flask(__name__)
CORS(app)

if not hasattr(app, 'metrics'):
    metrics = PrometheusMetrics(app, path='/metrics')
    app.metrics = metrics

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://user:password@db:5432/appdb'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)

class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    amount = db.Column(db.Float, nullable=False)
    currency = db.Column(db.String(10), nullable=False, default='USD')
    status = db.Column(db.String(20), nullable=False)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    order_number = db.Column(db.String(20), nullable=False)

with app.app_context():
    db.create_all()

@app.route('/pay', methods=['POST'])
def process_payment():
    data = request.get_json()
    user_id = data.get('user_id')
    amount = data.get('amount')
    currency = data.get('currency', 'USD')

    user = User.query.get(user_id)
    if user:
        try:
            order_number = f"ORD{random.randint(1000, 9999)}"
            new_payment = Payment(
                user_id=user_id,
                amount=amount,
                currency=currency,
                status='Completed',
                timestamp=datetime.utcnow(),
                order_number=order_number
            )
            db.session.add(new_payment)
            db.session.commit()
            return jsonify({"message": "Payment successful", "order_number": order_number, "payment_id": new_payment.id}), 201
        except Exception as e:
            db.session.rollback()
            return jsonify({"message": "Payment failed due to server error", "error": str(e)}), 500
    return jsonify({"message": "User not found"}), 404

@app.route('/payments/<int:user_id>', methods=['GET'])
def get_payments(user_id):
    try:
        payments = Payment.query.filter_by(user_id=user_id).all()
        return jsonify([{
            'order_number': payment.order_number,
            'amount': payment.amount,
            'currency': payment.currency,
            'timestamp': payment.timestamp
        } for payment in payments]), 200
    except Exception as e:
        return jsonify({"message": "Error fetching payments", "error": str(e)}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)
