from prometheus_client import start_http_server, Counter
import time

# Create a metric to track time spent and requests made.
REQUEST_COUNT = Counter('app_requests_count', 'Total number of requests to this web application')

# A function to simulate some work
def process_request():
    REQUEST_COUNT.inc()  # Increment the counter
    time.sleep(1)  # Simulate a delay

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate some requests.
    while True:
        process_request()
