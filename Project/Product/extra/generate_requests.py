import requests
import time
import random

url = 'http://127.0.0.1:3000'  # Replace with your Flask application's URL


def generate_traffic():
    while True:
        try:
            # Generate a random sleep interval between 0.1 and 2.0 seconds
            sleep_interval = random.uniform(0.1, 2.0)

            # Randomly select an HTTP method
            http_method = random.choice(['GET', 'POST', 'DELETE'])

            # Randomly generate a payload for POST and DELETE requests
            payload = {'key': 'value'} if http_method in ['POST', 'DELETE'] else None

            # Perform the HTTP request
            if http_method == 'GET':
                response = requests.get(url)
            elif http_method == 'POST':
                response = requests.post(url, json=payload)
            elif http_method == 'DELETE':
                response = requests.delete(url, json=payload)

            print(f"{http_method} request to {url} responded with status code {response.status_code}")

        except Exception as e:
            print(f"Request failed: {e}")

        # Introduce random traffic patterns by adjusting sleep intervals
        time.sleep(sleep_interval)


if __name__ == "__main__":
    generate_traffic()